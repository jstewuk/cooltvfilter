//
//  JSSDataSource.m
//  CoolTVFilter
//
//  Created by James Stewart on 1/14/13.
//  Copyright (c) 2013 StewartStuff. All rights reserved.
//

#import "JSSDataSource.h"

@interface JSSDataSource ()

@end

@implementation JSSDataSource

//subclass has to override and setup data
- (id)init {
    self = [super init];
    if (self) {
    }
    return self;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.data count];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:self.cellID];
    
    cell.textLabel.text = self.data[indexPath.row];
    
    return  cell;
}

@end
