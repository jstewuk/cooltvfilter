//
//  JSSViewController.m
//  CoolTVFilter
//
//  Created by James Stewart on 1/14/13.
//  Copyright (c) 2013  All rights reserved.
//

#import "JSSViewController.h"
#import "JSSInfo.h"
#import "JSSFilterSource.h"

@interface JSSViewController () <UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *infoTV;
@property (strong, nonatomic) IBOutlet UITableView *filterTV;
@property (nonatomic, strong) JSSInfo *infoDataSource;
@property (nonatomic, strong) JSSFilterSource *filterDataSource;
@property (nonatomic, weak) IBOutlet UIView *filterBar;
@property (weak, nonatomic) IBOutlet UILabel *activeFilterLabel;
@property (nonatomic, strong) NSLayoutConstraint *spaceBtwFilterBarAndMainTable;
@property (nonatomic, strong) NSArray *verticalConstraintsBeforeAnimation;
@property (nonatomic, strong) NSArray *verticalConstraintsAfterAnimation;
@property (nonatomic, strong) NSDictionary *viewsDictionary;
@end

@implementation JSSViewController 

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureFilterTV];
    self.filterTV.dataSource = self.filterDataSource;
    
    [self configureInfoTV];
    self.infoDataSource = [[JSSInfo alloc] init];
    self.infoTV.dataSource = self.infoDataSource;
    
    self.view.translatesAutoresizingMaskIntoConstraints = NO;
    
    self.filterBar.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addConstraint:self.spaceBtwFilterBarAndMainTable];

    [self.filterTV removeFromSuperview];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [self.filterTV removeFromSuperview]; // remove, but will it stay around
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBActions

- (IBAction)buttonPressed:(id)sender {
    if ([self.view.subviews containsObject:self.filterTV]) {
        [self hideFilterTable];
    } else {
        [self showFilterTable];
    }
}


#pragma mark - Helper/Internal Methods

- (void)configureFilterTV {
    [self.filterTV registerClass:[UITableViewCell class] forCellReuseIdentifier:@"FilterCell"];
    self.filterTV.delegate = self;
    self.filterTV.backgroundColor = [UIColor blackColor];
    self.filterTV.separatorColor = [UIColor darkGrayColor];
    self.filterTV.translatesAutoresizingMaskIntoConstraints = NO;
}

- (void)configureInfoTV {
    [self.infoTV registerClass:[UITableViewCell class] forCellReuseIdentifier:@"DataCell"];
    self.infoTV.delegate = self;
    self.infoTV.translatesAutoresizingMaskIntoConstraints = NO;
}

- (void)showFilterTable {
    
    [self.view addSubview:self.filterTV];
    
    NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[filterTV]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:self.viewsDictionary];
    [self.view addConstraints:constraints];
    
    [self.view removeConstraint:self.spaceBtwFilterBarAndMainTable];
    
    // set the pre animation view state
    [self.view addConstraints:self.verticalConstraintsBeforeAnimation];
    [self.view layoutIfNeeded];
    [self.view removeConstraints:self.verticalConstraintsBeforeAnimation];
    
    // set the post animation constraints but don't relayout..
    [self.view addConstraints:self.verticalConstraintsAfterAnimation];
    
    [UIView animateWithDuration:0.3f animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)hideFilterTable {
    [self.view removeConstraints:self.verticalConstraintsAfterAnimation];
    [self.view addConstraints:self.verticalConstraintsBeforeAnimation];
    
    [UIView
     animateWithDuration:0.3f animations:^{
         [self.view layoutIfNeeded];
     }
     completion:^(BOOL finished) {
         [self.filterTV removeFromSuperview];
         [self.view addConstraint:self.spaceBtwFilterBarAndMainTable];
     }];
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (tableView == self.filterTV) {
        self.activeFilterLabel.text = self.filterDataSource.data[indexPath.row];
        [self hideFilterTable];
    }
}

#pragma mark - Getters

- (id <UITableViewDataSource>)filterDataSource {
    if (! _filterDataSource) {
        _filterDataSource = [[JSSFilterSource alloc] init];
    }
    return _filterDataSource;
}

- (NSDictionary *)viewsDictionary {
    if ( ! _viewsDictionary) {
        _viewsDictionary = (@{
                                     @"filterTV" : self.filterTV,
                                     @"filterBar" : self.filterBar,
                            @"infoTV" : self.infoTV});
    }
    return _viewsDictionary;
}

- (NSLayoutConstraint *)spaceBtwFilterBarAndMainTable {
    if ( ! _spaceBtwFilterBarAndMainTable) {
       _spaceBtwFilterBarAndMainTable = [NSLayoutConstraint
                                      constraintWithItem:self.filterBar
                                      attribute:NSLayoutAttributeBottom
                                      relatedBy:NSLayoutRelationEqual
                                      toItem:self.infoTV
                                      attribute:NSLayoutAttributeTop multiplier:1.0f constant:0];
    }
    return _spaceBtwFilterBarAndMainTable;
}

- (NSArray *)verticalConstraintsBeforeAnimation {
    if (! _verticalConstraintsBeforeAnimation) {
        _verticalConstraintsBeforeAnimation = [NSLayoutConstraint
                                               constraintsWithVisualFormat:@"V:[filterBar][filterTV(0)][infoTV]"
                                               options:0
                                               metrics:nil
                                               views:self.viewsDictionary];
    }
    return _verticalConstraintsBeforeAnimation;
}

- (NSArray *)verticalConstraintsAfterAnimation {
    if (! _verticalConstraintsAfterAnimation) {
    _verticalConstraintsAfterAnimation = [NSLayoutConstraint
                   constraintsWithVisualFormat:@"V:[filterBar][filterTV(96)][infoTV]"
                   options:0
                   metrics:nil
                   views:self.viewsDictionary];
    }
    return _verticalConstraintsAfterAnimation;
 }


@end
