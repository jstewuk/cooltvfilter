//
//  main.m
//  CoolTVFilter
//
//  Created by James Stewart on 1/14/13.
//  Copyright (c) 2013 StewartStuff. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "JSSAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([JSSAppDelegate class]));
    }
}
