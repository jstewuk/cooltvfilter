//
//  JSSInfo.m
//  CoolTVFilter
//
//  Created by James Stewart on 1/14/13.
//  Copyright (c) 2013 StewartStuff. All rights reserved.
//

#import "JSSInfo.h"

@implementation JSSInfo

- (id)init {
    self = [super init];
    if (self) {
        self.data = (@[@"Row 1", @"Row 2", @"Row 3", @"Row 4", @"Row 5", @"Row 6", @"Row 7",
                        @"Row 8", @"Row 9", @"Row 10", @"Row 11", @"Row 12"]);
        self.cellID = @"DataCell";
    }
    return self;
}

@end
