//
//  JSSAppDelegate.h
//  CoolTVFilter
//
//  Created by James Stewart on 1/14/13.
//  Copyright (c) 2013 StewartStuff. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JSSViewController;

@interface JSSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) JSSViewController *viewController;

@end
