//
//  JSSDataSource.h
//  CoolTVFilter
//
//  Created by James Stewart on 1/14/13.
//  Copyright (c) 2013 StewartStuff. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JSSDataSource : NSObject <UITableViewDataSource>
@property (nonatomic, strong) NSArray *data;
@property (nonatomic, strong) NSString *cellID;
@end
