//
//  JSSFilterSource.m
//  CoolTVFilter
//
//  Created by James Stewart on 1/14/13.
//  Copyright (c) 2013 StewartStuff. All rights reserved.
//

#import "JSSFilterSource.h"

@implementation JSSFilterSource

- (id)init {
    self = [super init];
    if (self) {
        self.data = @[@"Filter 1", @"Filter 2", @"Filter 3", @"Filter 4", @"Filter 5", @"Filter 6", @"Filter 7"];
        self.cellID = @"FilterCell";
    }
    return self;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    cell.textLabel.textColor = [UIColor whiteColor];
    return cell;
}
@end
